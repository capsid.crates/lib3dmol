#![feature(test)]
extern crate lib3dmol;
extern crate test;

use test::Bencher;
use lib3dmol::parser;

#[bench]
fn parse_one_domain(b: &mut Bencher) {
    b.iter(|| {
        let _ = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
    });
}

#[bench]
fn parse_domain_adn(b: &mut Bencher) {
    b.iter(|| {
        let _ = parser::read_pdb("tests/tests_file/f2_adn.pdb", "f2");
    });
}

#[warn(non_snake_case)]
#[bench]
fn parse_large_MD_PDB(b: &mut Bencher) {
    b.iter(|| {
        let _ = parser::read_pdb("tests/tests_file/trp_MD.pdb", "TRP");
    });
}
