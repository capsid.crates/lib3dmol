/// Structure enum for the different pattern keywords.
/// Chain(begin, end) for the selection of proteic chain with names
/// Resid(begin, end)
/// Backbone

#[derive(Debug, PartialEq)]
pub enum Select {
    Chain(char, char),
    Resid(usize, usize),
    Backbone,
}

/// Parse option given for the selection of residues and return
/// the index of begin and end of select residues.
///
fn parse_options_int(opt: &[&str]) -> Option<[usize; 2]> {
    // if the len is 1 mean that the request is like "Resid 1".
    // return Some(1, 1)
    if opt.len() == 1 {
        match opt[0].parse() {
            Ok(v) => return Some([v, v]),
            Err(_) => return None,
        }
    }
    if opt.len() == 3 && opt[1] == "to" {
        let index1: usize = match opt[0].parse() {
            Ok(v) => v,
            Err(_) => return None,
        };
        let index2: usize = match opt[2].parse() {
            Ok(v) => v,
            Err(_) => return None,
        };
        return Some([index1, index2]);
    }
    None
}

/// Parse option given for the selection of atoms
///
/// TODO: Remove this one because it's a doublon of parse_option_int
///
fn parse_options_char(opt: &[&str]) -> Option<[char; 2]> {
    // if the len is 1 mean that the request is like "Chain A".
    // return Some(1, 1)
    if opt.len() == 1 {
        match opt[0].parse() {
            Ok(v) => return Some([v, v]),
            Err(_) => return None,
        }
    }
    if opt.len() == 3 && opt[1] == "to" {
        let index1: char = match opt[0].parse() {
            Ok(v) => v,
            Err(_) => return None,
        };
        let index2: char = match opt[2].parse() {
            Ok(v) => v,
            Err(_) => return None,
        };
        return Some([index1, index2]);
    }
    None
}

pub fn parse_select(pattern: &str) -> Option<Vec<Select>> {
    // The pattern is first split with "and" keyworkd into multiple sub-pattern (selection)
    // sub-pattern are then compared with "resid", "chain" and "backbone" keyworkds
    //
    // Return None if there is a probleme in the pattern parsing

    if pattern == "" {
        return None; //selection empty
    }

    let mut lst_selection: Vec<Select> = Vec::new(); //vector to return containing sub_patterns

    let sel: Vec<&str> = pattern.split("and").collect();

    for pattern in sel {
        let sub_pattern: Vec<&str> = pattern.split_whitespace().collect();

        match sub_pattern[0].to_lowercase().as_str() {
            "resid" => match parse_options_int(&sub_pattern[1..]) {
                Some(x) => lst_selection.push(Select::Resid(x[0], x[1])),
                None => return None,
            },
            "chain" => match parse_options_char(&sub_pattern[1..]) {
                Some(x) => lst_selection.push(Select::Chain(x[0], x[1])),
                None => return None,
            },
            "backbone" => lst_selection.push(Select::Backbone),
            _ => {
                println!("Error in the selection");
                return None;
            }
        }
    }
    Some(lst_selection)
}

pub fn atom_match(sel: &[Select], chain: char, res_id: u64, is_back: bool) -> bool {
    // For each pattern in sel, the pattern is compare to the caracteristics of the atom
    // if at any moment, the caracteristics are not ok, the function return false
    // In the end, it return true (consider everythings is ok)

    for pattern in sel {
        match pattern {
            Select::Chain(x, y) => {
                if !(chain >= *x && chain <= *y) {
                    return false;
                }
            }
            Select::Backbone => {
                if !(is_back) {
                    return false;
                }
            }
            Select::Resid(x, y) => {
                if !(res_id as usize >= *x && res_id as usize <= *y) {
                    return false;
                }
            }
        }
    }
    true
}

#[test]
fn test_atom_search() {
    let mut s = Vec::new();
    s.push(Select::Chain('B', 'C'));
    assert!(atom_match(&s, 'C', 15, true));

    s.push(Select::Resid(14, 155));
    assert!(atom_match(&s, 'C', 15, true));
    assert!(!atom_match(&s, 'C', 13, true));

    s.push(Select::Backbone);
    assert!(atom_match(&s, 'C', 15, true));
    assert!(!atom_match(&s, 'C', 15, false));
}

#[test]
fn test_parse_select() {
    assert_eq!(None, parse_select(""));
    let v = parse_select("resid 5 to 96").unwrap();
    println!("{:?}", v);
    assert_eq!(Select::Resid(5, 96), v[0]);

    let v = parse_select("resid 5 to 96 and chain B").unwrap();
    assert_eq!(Select::Resid(5, 96), v[0]);
    assert_eq!(Select::Chain('B', 'B'), v[1]);

    let v = parse_select("Select all the protein");
    assert_eq!(None, v);
}
