use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::process;

use crate::structures::atom::AtomType;
use crate::structures::structure::Structure;

/// Parse the string to return a f32. The `trim` is used to remove
/// /n and spaces.
///
/// # Errors
/// Will return 0.0 if the String cannot be convert and print the error
///
fn parse_float(s: &str) -> f32 {
    match s.trim().parse::<f32>() {
        Ok(n) => n,
        Err(e) => {
            println!("{}", e);
            0.0
        }
    }
}

/// Parse the string to return a i64. The `trim` is used to remove
/// /n and spaces.
/// In large PDB, atom number can be > 99,999.
/// In VMD, the atom number is in hexadecimal after 99,999. Using the
/// from_str_radix function to convert hexa -> i64
///
/// # Errors
/// Will return 0 if the String cannot be convert and print the error
///
fn parse_int(s: &str) -> i64 {
    match s.trim().parse::<i64>() {
        Ok(n) => n,
        Err(e) => match i64::from_str_radix(s.trim(), 16) {
            Ok(n) => n,
            Err(_) => {
                println!("{}", e);
                0
            }
        },
    }
}

/// Parse char to check if it is a alphanumeric character or not
/// Return an option<char>
///
fn parse_char(s: char) -> Option<char> {
    match s {
        c if c.is_ascii_alphanumeric() => Some(c),
        _ => None,
    }
}

/// Parse the pdb file and return a [`Structure`]
///
/// # Examples
/// ```
/// use lib3dmol::parser;
/// let my_struct = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
/// assert_eq!(66, my_struct.get_residue_number());
/// ```
pub fn read_pdb(pdb: &str, name: &str) -> Structure {
    // Check if the file exist and/or can be read
    let pdb = match File::open(pdb) {
        Ok(f) => f,
        Err(e) => {
            eprintln!("Could not open the file \"{}\"\nError: {}", pdb, e);
            process::exit(1);
        }
    };

    let reader = BufReader::new(pdb);
    let mut structure = Structure::new(name.to_string());

    for line in reader.lines() {
        let l = line.unwrap();
        update_from_line(&mut structure, &l);
    }
    structure
}

/// Read the text in the [`String`] or [`&str`] in the PDB format and return a Structure
///
/// # Examples
/// ```
/// use lib3dmol::parser;
/// let res = "ATOM      1  N   ALA     2      -0.677  -1.230  -0.491  1.00  0.00           N
/// ATOM      2  CA  ALA     2      -0.001   0.064  -0.491  1.00  0.00           C
/// ATOM      3  C   ALA     2       1.499  -0.110  -0.491  1.00  0.00           C
/// ATOM      4  O   ALA     2       2.030  -1.227  -0.502  1.00  0.00           O
/// ATOM      5  CB  ALA     2      -0.509   0.856   0.727  1.00  0.00           C
/// ATOM      6  H   ALA     2      -0.131  -2.162  -0.491  1.00  0.00           H
/// ATOM      7  HA  ALA     2      -0.269   0.603  -1.418  1.00  0.00           H
/// ATOM      8 1HB  ALA     2      -1.605   1.006   0.691  1.00  0.00           H
/// ATOM      9 2HB  ALA     2      -0.285   0.342   1.681  1.00  0.00           H
/// ATOM     10 3HB  ALA     2      -0.053   1.861   0.784  1.00  0.00           H
/// TER
/// END";
///
/// let my_alanine = parser::read_pdb_txt(res, "alanine");
///
/// assert_eq!(my_alanine.get_atom_number(), 10);
/// ```
pub fn read_pdb_txt(text: &str, name: &str) -> Structure {
    let mut structure = Structure::new(name.to_string());
    for line in text.lines() {
        update_from_line(&mut structure, &line);
    }
    structure
}

fn update_from_line(structure: &mut Structure, l: &str) {
    if l.starts_with("HETAM") || l.starts_with("ATOM") {
        // Set the 4 variables to their values per default
        // Edit them after if they are presents in the file
        let mut occupancy = None;
        let mut temp_factor = None;
        let mut element = None;
        let mut charge = None;

        // First get the resname.
        // If the "residue" is a amino acid, continue to parse the line and add informations to the protein
        // else continue to the next one line
        let residue_name = &l[17..20].trim();
        let atom_name = &l[12..16];
        let atom_type = parse_atom(&atom_name);
        let chain = l[21..22].chars().next().unwrap();
        let atom_number = parse_int(&l[6..11]);
        let residue_number = parse_int(&l[22..26]);
        let res_icode = parse_char(l.chars().nth(26).unwrap());
        let x = parse_float(&l[30..38]);
        let y = parse_float(&l[38..46]);
        let z = parse_float(&l[46..54]);

        // Attribute values according to the length of the line
        match l.len() {
            x if x > 60 => occupancy = Some(parse_float(&l[54..60])),
            x if x > 66 => temp_factor = Some(parse_float(&l[60..66])),
            x if x > 78 => element = Some(l[76..78].to_string()),
            x if x > 80 => charge = Some(l[78..80].to_string()),
            _ => (), // Not reachable
        };
        // Add informations to the Structure
        structure.update_structure(
            chain,
            residue_name.to_string(),
            residue_number as u64,
            res_icode,
            atom_name.to_string(),
            atom_number as u64,
            atom_type,
            [x, y, z],
            occupancy,
            temp_factor,
            element,
            charge,
        );
    }
}

/// Parse a 4 char string lenght corresponding  at the column 12 to 16 in the PDB format
/// These columns contains informations on the Atom type
/// The symbol of the atom is in the 2 first char exepting for Hydrogen atoms
fn parse_atom(txt: &str) -> AtomType {
    let symbol = &txt[0..2];

    match symbol {
        " H" => AtomType::Hydrogen,
        "LI" => AtomType::Lithium,
        "BE" => AtomType::Beryllium,
        " B" => AtomType::Boron,
        " C" => AtomType::Carbon,
        " N" => AtomType::Nitrogen,
        " O" => AtomType::Oxygen,
        " F" => AtomType::Fluorine,
        "NE" => AtomType::Neon,
        "NA" => AtomType::Sodium,
        "MG" => AtomType::Magnesium,
        "AL" => AtomType::Aluminum,
        "SI" => AtomType::Silicon,
        " P" => AtomType::Phosphorus,
        " S" => AtomType::Sulfur,
        "CL" => AtomType::Chlorine,
        "AR" => AtomType::Argon,
        " K" => AtomType::Potassium,
        "CA" => AtomType::Calcium,
        "SC" => AtomType::Scandium,
        "TI" => AtomType::Titanium,
        " V" => AtomType::Vanadium,
        "CR" => AtomType::Chromium,
        "MN" => AtomType::Manganese,
        "FE" => AtomType::Iron,
        "CO" => AtomType::Cobalt,
        "NI" => AtomType::Nickel,
        "CU" => AtomType::Copper,
        "ZN" => AtomType::Zinc,
        "GA" => AtomType::Gallium,
        "GE" => AtomType::Germanium,
        "AS" => AtomType::Arsenic,
        "SE" => AtomType::Selenium,
        "BR" => AtomType::Bromine,
        "KR" => AtomType::Krypton,
        "RB" => AtomType::Rubidium,
        "SR" => AtomType::Strontium,
        " Y" => AtomType::Yttrium,
        "ZR" => AtomType::Zirconium,
        "NB" => AtomType::Niobium,
        "MO" => AtomType::Molybdenum,
        "TC" => AtomType::Technetium,
        "RU" => AtomType::Ruthenium,
        "RH" => AtomType::Rhodium,
        "PD" => AtomType::Palladium,
        "AG" => AtomType::Silver,
        "CD" => AtomType::Cadmium,
        "IN" => AtomType::Indium,
        "SN" => AtomType::Tin,
        "SB" => AtomType::Antimony,
        "TE" => AtomType::Tellurium,
        " I" => AtomType::Iodine,
        "XE" => AtomType::Xenon,
        "CS" => AtomType::Cesium,
        "BA" => AtomType::Barium,
        "LA" => AtomType::Lanthanum,
        "CE" => AtomType::Cerium,
        "PR" => AtomType::Praseodymium,
        "ND" => AtomType::Neodymium,
        "PM" => AtomType::Promethium,
        "SM" => AtomType::Samarium,
        "EU" => AtomType::Europium,
        "GD" => AtomType::Gadolinium,
        "TB" => AtomType::Terbium,
        "DY" => AtomType::Dysprosium,
        "ER" => AtomType::Erbium,
        "TM" => AtomType::Thulium,
        "YB" => AtomType::Ytterbium,
        "LU" => AtomType::Lutetium,
        "TA" => AtomType::Tantalum,
        " W" => AtomType::Tungsten,
        "RE" => AtomType::Rhenium,
        "OS" => AtomType::Osmium,
        "IR" => AtomType::Iridium,
        "PT" => AtomType::Platinum,
        "AU" => AtomType::Gold,
        "TL" => AtomType::Thallium,
        "PB" => AtomType::Lead,
        "BI" => AtomType::Bismuth,
        "PO" => AtomType::Polonium,
        "AT" => AtomType::Astatine,
        "RN" => AtomType::Radon,
        "FR" => AtomType::Francium,
        "RA" => AtomType::Radium,
        "AC" => AtomType::Actinium,
        "TH" => AtomType::Thorium,
        "PA" => AtomType::Protactinium,
        " U" => AtomType::Uranium,
        "NP" => AtomType::Neptunium,
        "PU" => AtomType::Plutonium,
        "AM" => AtomType::Americium,
        "CM" => AtomType::Curium,
        "BK" => AtomType::Berkelium,
        "CF" => AtomType::Californium,
        "ES" => AtomType::Einsteinium,
        "FM" => AtomType::Fermium,
        "MD" => AtomType::Mendelevium,
        "NO" => AtomType::Nobelium,
        "LR" => AtomType::Lawrencium,
        "RF" => AtomType::Rutherfordium,
        "DB" => AtomType::Dubnium,
        "SG" => AtomType::Seaborgium,
        "BH" => AtomType::Bohrium,
        "MT" => AtomType::Meitnerium,
        _ => {
            let next_chars: Vec<char> = txt.chars().collect();
            match next_chars[1] {
                _ if next_chars[1].is_digit(10) => AtomType::Hydrogen,
                _ if next_chars[2].is_digit(10) => AtomType::Hydrogen,
                'G' => AtomType::Mercury,
                'E' => AtomType::Helium,
                'O' => AtomType::Holmium,
                'F' => AtomType::Hafnium,
                'S' => AtomType::Hassium,
                _ => AtomType::Unknown,
            }
        }
    }
}
