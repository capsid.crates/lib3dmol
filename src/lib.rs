//! # Lib3Dmol
//!
//! `Lib3Dmol` is a library to parser, filter, create, edit and convert molecules in PDB format.
//! It provide functions to filter atoms according to text pattern matching, compute distances between structures and sub-structures.
//! A file is represented as a [`Structure`]
//! You can create a [Structure] by parsing with [`read_pdb`] function. Then you can add filters on your Structure.
//! And save it with write_pdb function.
//!
//! # Examples
//! ```rust
//! use lib3dmol::parser;
//!
//! fn main() {
//!     let my_structure = parser::read_pdb("tests/tests_file/f2.pdb", "Protein f2");
//!
//!     println!(
//!         "Structure name: {}
//! Number of chain: {}
//! Number of residue: {}
//! Number of atom: {}",
//!         my_structure.name,
//!         my_structure.get_chain_number(),
//!         my_structure.get_residue_number(),
//!         my_structure.get_atom_number()
//!     );
//!
//!     // Now we will extract the backbone
//!
//!     let backbone = my_structure.select_atoms("backbone").unwrap();
//!
//!     println!(
//!         "Number of chain: {}
//! Number of residue: {}
//! Number of atom: {}",
//!         backbone.get_chain_number(),
//!         backbone.get_residue_number(),
//!         backbone.get_atom_number()
//!     );
//! }
//! ```
#[macro_use]
extern crate lazy_static;

pub mod build;
pub mod parser;
pub mod structures;
pub mod tools;
pub mod writer;
