//! Build amino acid as a [`Structure`]
//! Can be used directly with lib3dmol::build::build_residue

use crate::parser;
use crate::structures::{residue, structure};
use residue::ResidueType;

/// Build a residue according to its [`ResidueType`] and return a [`Structure`]
///
/// # Examples
/// ```
/// use lib3dmol::{structures, build};
///
/// let trp = build::build_residue(structures::residue::ResidueType::Trp);
/// assert_eq!(trp.get_atom_number(), 24);
/// assert_eq!(trp.get_residue_number(), 1);
/// ```
pub fn build_residue(res_type: residue::ResidueType) -> structure::Structure {
    match res_type {
        ResidueType::Ala => alanine(),
        ResidueType::Arg => arginine(),
        ResidueType::Asn => asparagine(),
        ResidueType::Asp => aspartate(),
        ResidueType::Cys => cysteine(),
        ResidueType::Glu => glutamate(),
        ResidueType::Gln => glutamine(),
        ResidueType::Gly => glycine(),
        ResidueType::His => histidine(),
        ResidueType::Ile => isoleucine(),
        ResidueType::Leu => leucine(),
        ResidueType::Lys => lysine(),
        ResidueType::Met => methionine(),
        ResidueType::Phe => phenylalanine(),
        ResidueType::Pro => proline(),
        ResidueType::Ser => serine(),
        ResidueType::Thr => threonine(),
        ResidueType::Trp => tryptophane(),
        ResidueType::Tyr => tyrosine(),
        ResidueType::Val => valine(),
    }
}

fn alanine() -> structure::Structure {
    let res = "ATOM      1  N   ALA     2      -0.677  -1.230  -0.491  1.00  0.00           N
ATOM      2  CA  ALA     2      -0.001   0.064  -0.491  1.00  0.00           C
ATOM      3  C   ALA     2       1.499  -0.110  -0.491  1.00  0.00           C
ATOM      4  O   ALA     2       2.030  -1.227  -0.502  1.00  0.00           O
ATOM      5  CB  ALA     2      -0.509   0.856   0.727  1.00  0.00           C
ATOM      6  H   ALA     2      -0.131  -2.162  -0.491  1.00  0.00           H
ATOM      7  HA  ALA     2      -0.269   0.603  -1.418  1.00  0.00           H
ATOM      8 1HB  ALA     2      -1.605   1.006   0.691  1.00  0.00           H
ATOM      9 2HB  ALA     2      -0.285   0.342   1.681  1.00  0.00           H
ATOM     10 3HB  ALA     2      -0.053   1.861   0.784  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Alanine")
}

fn arginine() -> structure::Structure {
    let res = "ATOM      1  N   ARG     2      -0.022  -3.647   2.514  1.00  0.00           N
ATOM      2  CA  ARG     2       0.519  -2.968   1.340  1.00  0.00           C
ATOM      3  C   ARG     2       2.029  -2.951   1.374  1.00  0.00           C
ATOM      4  O   ARG     2       2.666  -3.478   2.283  1.00  0.00           O
ATOM      5  CB  ARG     2      -0.063  -1.527   1.282  1.00  0.00           C
ATOM      6  CG  ARG     2       0.188  -0.757  -0.045  1.00  0.00           C
ATOM      7  CD  ARG     2      -0.388   0.668  -0.004  1.00  0.00           C
ATOM      8  NE  ARG     2      -0.136   1.349  -1.308  1.00  0.00           N
ATOM      9  CZ  ARG     2      -0.497   2.593  -1.608  1.00  0.00           C
ATOM     10  NH1 ARG     2      -1.121   3.383  -0.780  1.00  0.00           N1+
ATOM     11  NH2 ARG     2      -0.215   3.048  -2.788  1.00  0.00           N
ATOM     12  H   ARG     2       0.584  -4.045   3.240  1.00  0.00           H
ATOM     13  HA  ARG     2       0.211  -3.538   0.443  1.00  0.00           H
ATOM     14 2HB  ARG     2       0.336  -0.947   2.140  1.00  0.00           H
ATOM     15 3HB  ARG     2      -1.156  -1.569   1.465  1.00  0.00           H
ATOM     16 2HG  ARG     2      -0.252  -1.330  -0.887  1.00  0.00           H
ATOM     17 3HG  ARG     2       1.276  -0.706  -0.255  1.00  0.00           H
ATOM     18 2HD  ARG     2       0.080   1.230   0.834  1.00  0.00           H
ATOM     19 3HD  ARG     2      -1.476   0.622   0.217  1.00  0.00           H
ATOM     20  HE  ARG     2       0.348   0.865  -2.071  1.00  0.00           H
ATOM     21 1HH1 ARG     2      -1.312   2.964   0.133  1.00  0.00           H
ATOM     22 2HH1 ARG     2      -1.365   4.326  -1.087  1.00  0.00           H
ATOM     23 1HH2 ARG     2       0.275   2.415  -3.423  1.00  0.00           H
ATOM     24 2HH2 ARG     2      -0.502   4.005  -3.000  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Arginine")
}

fn asparagine() -> structure::Structure {
    let res = "ATOM      1  N   ASN     2       0.209  -1.748  -0.613  1.00  0.00           N
ATOM      2  CA  ASN     2       0.793  -0.410  -0.613  1.00  0.00           C
ATOM      3  C   ASN     2       2.302  -0.479  -0.613  1.00  0.00           C
ATOM      4  O   ASN     2       2.906  -1.551  -0.613  1.00  0.00           O
ATOM      5  CB  ASN     2       0.278   0.381   0.624  1.00  0.00           C
ATOM      6  CG  ASN     2      -1.240   0.532   0.768  1.00  0.00           C
ATOM      7  ND2 ASN     2      -1.857   1.423   0.039  1.00  0.00           N
ATOM      8  OD1 ASN     2      -1.895  -0.168   1.526  1.00  0.00           O
ATOM      9  H   ASN     2       0.819  -2.639  -0.613  1.00  0.00           H
ATOM     10  HA  ASN     2       0.489   0.105  -1.543  1.00  0.00           H
ATOM     11 2HB  ASN     2       0.730   1.390   0.652  1.00  0.00           H
ATOM     12 3HB  ASN     2       0.639  -0.106   1.551  1.00  0.00           H
ATOM     13 1HD2 ASN     2      -2.875   1.338   0.098  1.00  0.00           H
ATOM     14 2HD2 ASN     2      -1.296   1.927  -0.647  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Asparagine")
}

fn aspartate() -> structure::Structure {
    let res = "ATOM      1  N   ASP     2      -0.610  -1.825  -0.630  1.00  0.00           N
ATOM      2  CA  ASP     2      -0.051  -0.476  -0.630  1.00  0.00           C
ATOM      3  C   ASP     2       1.458  -0.515  -0.630  1.00  0.00           C
ATOM      4  O   ASP     2       2.088  -1.584  -0.630  1.00  0.00           O
ATOM      5  CB  ASP     2      -0.586   0.337   0.582  1.00  0.00           C
ATOM      6  CG  ASP     2      -0.124   1.801   0.652  1.00  0.00           C
ATOM      7  OD1 ASP     2      -0.007   2.453  -0.406  1.00  0.00           O
ATOM      8  OD2 ASP     2       0.165   2.283   1.767  1.00  0.00           O1-
ATOM      9  H   ASP     2       0.017  -2.704  -0.630  1.00  0.00           H
ATOM     10  HA  ASP     2      -0.366   0.030  -1.563  1.00  0.00           H
ATOM     11 2HB  ASP     2      -0.291  -0.154   1.527  1.00  0.00           H
ATOM     12 3HB  ASP     2      -1.688   0.354   0.586  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Aspartate")
}

fn cysteine() -> structure::Structure {
    let res = "ATOM      1  N   CYS     2      -0.664  -1.578  -0.633  1.00  0.00           N
ATOM      2  CA  CYS     2      -0.105  -0.229  -0.633  1.00  0.00           C
ATOM      3  C   CYS     2       1.405  -0.269  -0.633  1.00  0.00           C
ATOM      4  O   CYS     2       2.034  -1.337  -0.633  1.00  0.00           O
ATOM      5  CB  CYS     2      -0.674   0.537   0.577  1.00  0.00           C
ATOM      6  SG  CYS     2      -0.163   2.270   0.535  1.00  0.00           S
ATOM      7  H   CYS     2      -0.036  -2.457  -0.633  1.00  0.00           H
ATOM      8  HA  CYS     2      -0.420   0.277  -1.564  1.00  0.00           H
ATOM      9 2HB  CYS     2      -0.349   0.082   1.535  1.00  0.00           H
ATOM     10 3HB  CYS     2      -1.778   0.509   0.585  1.00  0.00           H
ATOM     11  HG  CYS     2       0.749   2.191   1.501  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Cysteine")
}

fn glutamate() -> structure::Structure {
    let res = "ATOM      1  N   GLU     2      -0.517  -2.346  -1.038  1.00  0.00           N
ATOM      2  CA  GLU     2       0.042  -0.997  -1.038  1.00  0.00           C
ATOM      3  C   GLU     2       1.551  -1.037  -1.038  1.00  0.00           C
ATOM      4  O   GLU     2       2.180  -2.105  -1.038  1.00  0.00           O
ATOM      5  CB  GLU     2      -0.481  -0.212   0.199  1.00  0.00           C
ATOM      6  CG  GLU     2      -0.006   1.273   0.310  1.00  0.00           C
ATOM      7  CD  GLU     2      -0.430   2.077   1.525  1.00  0.00           C
ATOM      8  OE1 GLU     2      -1.158   1.575   2.403  1.00  0.00           O
ATOM      9  OE2 GLU     2      -0.000   3.247   1.597  1.00  0.00           O1-
ATOM     10  H   GLU     2       0.110  -3.225  -1.038  1.00  0.00           H
ATOM     11  HA  GLU     2      -0.284  -0.479  -1.958  1.00  0.00           H
ATOM     12 2HB  GLU     2      -0.192  -0.743   1.129  1.00  0.00           H
ATOM     13 3HB  GLU     2      -1.588  -0.222   0.212  1.00  0.00           H
ATOM     14 2HG  GLU     2      -0.316   1.858  -0.571  1.00  0.00           H
ATOM     15 3HG  GLU     2       1.094   1.340   0.339  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Glutamate")
}

fn glutamine() -> structure::Structure {
    let res = "ATOM      1  N   GLN     2      -0.311  -2.482  -1.387  1.00  0.00           N
ATOM      2  CA  GLN     2       0.241  -1.131  -1.387  1.00  0.00           C
ATOM      3  C   GLN     2       1.751  -1.162  -1.387  1.00  0.00           C
ATOM      4  O   GLN     2       2.380  -2.218  -1.387  1.00  0.00           O
ATOM      5  CB  GLN     2      -0.297  -0.376  -0.151  1.00  0.00           C
ATOM      6  CG  GLN     2       0.152   1.115  -0.003  1.00  0.00           C
ATOM      7  CD  GLN     2      -0.293   1.910   1.231  1.00  0.00           C
ATOM      8  NE2 GLN     2      -1.052   1.345   2.135  1.00  0.00           N
ATOM      9  OE1 GLN     2       0.069   3.064   1.402  1.00  0.00           O
ATOM     10  H   GLN     2       0.251  -3.261  -1.387  1.00  0.00           H
ATOM     11  HA  GLN     2      -0.083  -0.615  -2.310  1.00  0.00           H
ATOM     12 2HB  GLN     2       0.002  -0.934   0.761  1.00  0.00           H
ATOM     13 3HB  GLN     2      -1.404  -0.411  -0.164  1.00  0.00           H
ATOM     14 2HG  GLN     2      -0.150   1.693  -0.896  1.00  0.00           H
ATOM     15 3HG  GLN     2       1.255   1.185   0.008  1.00  0.00           H
ATOM     16 1HE2 GLN     2      -1.249   1.921   2.955  1.00  0.00           H
ATOM     17 2HE2 GLN     2      -1.262   0.359   1.967  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Glutamine")
}

fn glycine() -> structure::Structure {
    let res = "ATOM      1  N   GLY    22      -1.195   0.201  -0.206  1.00  0.00           N
ATOM      2  CA  GLY    22       0.230   0.318  -0.502  1.00  0.00           C
ATOM      3  C   GLY    22       1.059  -0.390   0.542  1.00  0.00           C
ATOM      4  O   GLY    22       0.545  -0.975   1.499  1.00  0.00           O
ATOM      5  H   GLY    22      -1.558  -0.333   0.660  1.00  0.00           H
ATOM      6 3HA  GLY    22       0.482   1.337  -0.514  0.00  0.00           H
ATOM      7  HA  GLY    22       0.434  -0.159  -1.479  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Glycine")
}

fn histidine() -> structure::Structure {
    let res = "ATOM      1  N   HIS     2       0.498  -2.372  -1.127  1.00  0.00           N
ATOM      2  CA  HIS     2       1.052  -1.053  -0.834  1.00  0.00           C
ATOM      3  C   HIS     2       2.561  -1.080  -0.865  1.00  0.00           C
ATOM      4  O   HIS     2       3.195  -2.117  -1.112  1.00  0.00           O
ATOM      5  CB  HIS     2       0.551  -0.676   0.570  1.00  0.00           C
ATOM      6  CG  HIS     2      -0.281   0.573   0.562  1.00  0.00           C
ATOM      7  CD2 HIS     2      -1.660   0.555   0.400  1.00  0.00           C
ATOM      8  ND1 HIS     2       0.197   1.873   0.711  1.00  0.00           N
ATOM      9  CE1 HIS     2      -0.959   2.556   0.620  1.00  0.00           C
ATOM     10  NE2 HIS     2      -2.108   1.850   0.438  1.00  0.00           N
ATOM     11  H   HIS     2       1.129  -3.224  -1.332  1.00  0.00           H
ATOM     12  HA  HIS     2       0.732  -0.370  -1.639  1.00  0.00           H
ATOM     13 2HB  HIS     2       1.397  -0.517   1.268  1.00  0.00           H
ATOM     14 3HB  HIS     2      -0.059  -1.483   1.027  1.00  0.00           H
ATOM     15 2HD  HIS     2      -2.252  -0.346   0.268  1.00  0.00           H
ATOM     16 1HE  HIS     2      -0.912   3.634   0.700  1.00  0.00           H
ATOM     17 2HE  HIS     2      -3.075   2.194   0.348  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Histidine")
}

fn isoleucine() -> structure::Structure {
    let res = "ATOM      1  N   ILE     2       0.394  -2.167  -1.142  1.00  0.00           N
ATOM      2  CA  ILE     2       0.891  -0.794  -1.142  1.00  0.00           C
ATOM      3  C   ILE     2       2.400  -0.765  -1.142  1.00  0.00           C
ATOM      4  O   ILE     2       3.077  -1.804  -1.142  1.00  0.00           O
ATOM      5  CB  ILE     2       0.286   0.038   0.061  1.00  0.00           C
ATOM      6  CG1 ILE     2      -1.272  -0.010   0.168  1.00  0.00           C
ATOM      7  CG2 ILE     2       0.725   1.535   0.043  1.00  0.00           C
ATOM      8  CD1 ILE     2      -1.872   0.510   1.491  1.00  0.00           C
ATOM      9  H   ILE     2       1.060  -3.017  -1.142  1.00  0.00           H
ATOM     10  HA  ILE     2       0.568  -0.322  -2.088  1.00  0.00           H
ATOM     11  HB  ILE     2       0.684  -0.416   0.993  1.00  0.00           H
ATOM     12 2HG1 ILE     2      -1.618  -1.056   0.070  1.00  0.00           H
ATOM     13 3HG1 ILE     2      -1.732   0.515  -0.692  1.00  0.00           H
ATOM     14 1HG2 ILE     2       1.820   1.668   0.087  1.00  0.00           H
ATOM     15 2HG2 ILE     2       0.366   2.063  -0.861  1.00  0.00           H
ATOM     16 3HG2 ILE     2       0.345   2.096   0.916  1.00  0.00           H
ATOM     17 1HD1 ILE     2      -1.461  -0.027   2.367  1.00  0.00           H
ATOM     18 2HD1 ILE     2      -1.686   1.588   1.646  1.00  0.00           H
ATOM     19 3HD1 ILE     2      -2.969   0.373   1.513  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Isoleucine")
}

fn leucine() -> structure::Structure {
    let res = "ATOM      1  N   LEU     2       0.678  -2.128  -0.894  1.00  0.00           N
ATOM      2  CA  LEU     2       1.246  -0.783  -0.894  1.00  0.00           C
ATOM      3  C   LEU     2       2.755  -0.834  -0.894  1.00  0.00           C
ATOM      4  O   LEU     2       3.377  -1.907  -0.894  1.00  0.00           O
ATOM      5  CB  LEU     2       0.718   0.033   0.320  1.00  0.00           C
ATOM      6  CG  LEU     2      -0.817   0.218   0.464  1.00  0.00           C
ATOM      7  CD1 LEU     2      -1.129   1.139   1.652  1.00  0.00           C
ATOM      8  CD2 LEU     2      -1.470   0.778  -0.811  1.00  0.00           C
ATOM      9  H   LEU     2       1.267  -2.967  -0.894  1.00  0.00           H
ATOM     10  HA  LEU     2       0.953  -0.280  -1.833  1.00  0.00           H
ATOM     11 2HB  LEU     2       1.188   1.036   0.300  1.00  0.00           H
ATOM     12 3HB  LEU     2       1.097  -0.432   1.252  1.00  0.00           H
ATOM     13  HG  LEU     2      -1.266  -0.775   0.676  1.00  0.00           H
ATOM     14 1HD1 LEU     2      -0.710   0.746   2.598  1.00  0.00           H
ATOM     15 2HD1 LEU     2      -0.719   2.157   1.512  1.00  0.00           H
ATOM     16 3HD1 LEU     2      -2.218   1.245   1.810  1.00  0.00           H
ATOM     17 1HD2 LEU     2      -1.017   1.735  -1.128  1.00  0.00           H
ATOM     18 2HD2 LEU     2      -1.371   0.071  -1.656  1.00  0.00           H
ATOM     19 3HD2 LEU     2      -2.555   0.947  -0.680  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Leucine")
}

fn lysine() -> structure::Structure {
    let res = "ATOM      1  N   LYS     2      -0.449  -3.572  -1.666  1.00  0.00           N
ATOM      2  CA  LYS     2       0.110  -2.223  -1.666  1.00  0.00           C
ATOM      3  C   LYS     2       1.620  -2.263  -1.666  1.00  0.00           C
ATOM      4  O   LYS     2       2.249  -3.331  -1.666  1.00  0.00           O
ATOM      5  CB  LYS     2      -0.425  -1.442  -0.435  1.00  0.00           C
ATOM      6  CG  LYS     2       0.052   0.031  -0.385  1.00  0.00           C
ATOM      7  CD  LYS     2      -0.403   0.814   0.850  1.00  0.00           C
ATOM      8  CE  LYS     2       0.198   2.225   0.804  1.00  0.00           C
ATOM      9  NZ  LYS     2      -0.372   3.036   1.894  1.00  0.00           N1+
ATOM     10  H   LYS     2       0.179  -4.451  -1.666  1.00  0.00           H
ATOM     11  HA  LYS     2      -0.209  -1.713  -2.594  1.00  0.00           H
ATOM     12 2HB  LYS     2      -0.128  -1.966   0.497  1.00  0.00           H
ATOM     13 3HB  LYS     2      -1.533  -1.464  -0.432  1.00  0.00           H
ATOM     14 2HG  LYS     2      -0.270   0.557  -1.305  1.00  0.00           H
ATOM     15 3HG  LYS     2       1.157   0.075  -0.392  1.00  0.00           H
ATOM     16 2HD  LYS     2      -0.075   0.288   1.770  1.00  0.00           H
ATOM     17 3HD  LYS     2      -1.509   0.846   0.887  1.00  0.00           H
ATOM     18 2HE  LYS     2      -0.007   2.716  -0.170  1.00  0.00           H
ATOM     19 3HE  LYS     2       1.304   2.179   0.893  1.00  0.00           H
ATOM     20 1HZ  LYS     2      -0.508   2.465   2.737  1.00  0.00           H
ATOM     21 2HZ  LYS     2       0.261   3.796   2.164  1.00  0.00           H
ATOM     22 3HZ  LYS     2      -1.247   3.402   1.548  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Lysine")
}

fn methionine() -> structure::Structure {
    let res = "ATOM      1  N   MET     2       1.052  -1.937  -1.166  1.00  0.00           N
ATOM      2  CA  MET     2       1.540  -0.561  -1.166  1.00  0.00           C
ATOM      3  C   MET     2       3.049  -0.521  -1.166  1.00  0.00           C
ATOM      4  O   MET     2       3.733  -1.556  -1.166  1.00  0.00           O
ATOM      5  CB  MET     2       0.965   0.202   0.059  1.00  0.00           C
ATOM      6  CG  MET     2      -0.571   0.352   0.100  1.00  0.00           C
ATOM      7  SD  MET     2      -1.038   1.496   1.409  1.00  0.00           S
ATOM      8  CE  MET     2      -2.801   1.147   1.451  1.00  0.00           C
ATOM      9  H   MET     2       1.725  -2.782  -1.166  1.00  0.00           H
ATOM     10  HA  MET     2       1.202  -0.067  -2.096  1.00  0.00           H
ATOM     11 2HB  MET     2       1.403   1.218   0.096  1.00  0.00           H
ATOM     12 3HB  MET     2       1.300  -0.285   0.997  1.00  0.00           H
ATOM     13 2HG  MET     2      -1.043  -0.631   0.287  1.00  0.00           H
ATOM     14 3HG  MET     2      -0.970   0.718  -0.864  1.00  0.00           H
ATOM     15 1HE  MET     2      -3.258   1.295   0.456  1.00  0.00           H
ATOM     16 2HE  MET     2      -2.979   0.103   1.764  1.00  0.00           H
ATOM     17 3HE  MET     2      -3.310   1.813   2.169  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Methionine")
}

fn phenylalanine() -> structure::Structure {
    let res = "ATOM      1  N   PHE     2       1.378  -1.795  -1.043  1.00  0.00           N
ATOM      2  CA  PHE     2       1.893  -0.429  -1.043  1.00  0.00           C
ATOM      3  C   PHE     2       3.403  -0.419  -1.043  1.00  0.00           C
ATOM      4  O   PHE     2       4.078  -1.486  -1.043  1.00  0.00           O
ATOM      5  CB  PHE     2       1.385   0.334   0.218  1.00  0.00           C
ATOM      6  CG  PHE     2      -0.139   0.429   0.393  1.00  0.00           C
ATOM      7  CD1 PHE     2      -0.821  -0.586   1.074  1.00  0.00           C
ATOM      8  CD2 PHE     2      -0.860   1.483  -0.173  1.00  0.00           C
ATOM      9  CE1 PHE     2      -2.208  -0.556   1.171  1.00  0.00           C
ATOM     10  CE2 PHE     2      -2.248   1.518  -0.066  1.00  0.00           C
ATOM     11  CZ  PHE     2      -2.921   0.497   0.603  1.00  0.00           C
ATOM     12  H   PHE     2       2.033  -2.653  -1.043  1.00  0.00           H
ATOM     13  HA  PHE     2       1.547   0.085  -1.960  1.00  0.00           H
ATOM     14 2HB  PHE     2       1.801   1.361   0.219  1.00  0.00           H
ATOM     15 3HB  PHE     2       1.822  -0.122   1.130  1.00  0.00           H
ATOM     16 1HD  PHE     2      -0.276  -1.423   1.490  1.00  0.00           H
ATOM     17 2HD  PHE     2      -0.346   2.267  -0.712  1.00  0.00           H
ATOM     18 1HE  PHE     2      -2.727  -1.360   1.672  1.00  0.00           H
ATOM     19 2HE  PHE     2      -2.802   2.331  -0.512  1.00  0.00           H
ATOM     20  HZ  PHE     2      -3.998   0.515   0.675  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Phenylalanine")
}

fn proline() -> structure::Structure {
    let res = "ATOM      1  N   PRO     2       0.649  -0.549   1.064  1.00  0.00           N
ATOM      2  CA  PRO     2       1.159   0.454   0.141  1.00  0.00           C
ATOM      3  C   PRO     2       1.064   1.870   0.735  1.00  0.00           C
ATOM      4  O   PRO     2       0.607   2.044   1.864  1.00  0.00           O
ATOM      5  CB  PRO     2       0.249   0.252  -1.080  1.00  0.00           C
ATOM      6  CG  PRO     2      -1.068  -0.252  -0.471  1.00  0.00           C
ATOM      7  CD  PRO     2      -0.604  -1.157   0.674  1.00  0.00           C
ATOM      8  HA  PRO     2       2.202   0.248  -0.110  1.00  0.00           H
ATOM      9 2HB  PRO     2       0.674  -0.529  -1.713  1.00  0.00           H
ATOM     10 3HB  PRO     2       0.122   1.162  -1.671  1.00  0.00           H
ATOM     11 2HG  PRO     2      -1.687  -0.787  -1.194  1.00  0.00           H
ATOM     12 3HG  PRO     2      -1.629   0.591  -0.063  1.00  0.00           H
ATOM     13 2HD  PRO     2      -0.421  -2.174   0.318  1.00  0.00           H
ATOM     14 3HD  PRO     2      -1.313  -1.168   1.504  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Proline")
}

fn serine() -> structure::Structure {
    let res = "ATOM      1  N   SER     2      -0.621  -1.240  -0.688  1.00  0.00           N
ATOM      2  CA  SER     2       0.009   0.077  -0.688  1.00  0.00           C
ATOM      3  C   SER     2       1.515  -0.044  -0.688  1.00  0.00           C
ATOM      4  O   SER     2       2.081  -1.137  -0.688  1.00  0.00           O
ATOM      5  CB  SER     2      -0.494   0.929   0.504  1.00  0.00           C
ATOM      6  OG  SER     2      -0.029   0.446   1.769  1.00  0.00           O
ATOM      7  H   SER     2      -0.043  -2.152  -0.688  1.00  0.00           H
ATOM      8  HA  SER     2      -0.271   0.589  -1.627  1.00  0.00           H
ATOM      9 2HB  SER     2      -1.599   0.989   0.515  1.00  0.00           H
ATOM     10 3HB  SER     2      -0.153   1.976   0.384  1.00  0.00           H
ATOM     11  HG  SER     2      -0.393  -0.434   1.892  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Serine")
}

fn threonine() -> structure::Structure {
    let res = "ATOM      1  N   THR     2      -0.521  -2.002  -0.796  1.00  0.00           N
ATOM      2  CA  THR     2       0.097  -0.680  -0.796  1.00  0.00           C
ATOM      3  C   THR     2       1.603  -0.786  -0.796  1.00  0.00           C
ATOM      4  O   THR     2       2.172  -1.859  -0.796  1.00  0.00           O
ATOM      5  CB  THR     2      -0.399   0.159   0.429  1.00  0.00           C
ATOM      6  CG2 THR     2       0.182   1.583   0.564  1.00  0.00           C
ATOM      7  OG1 THR     2      -1.806   0.353   0.365  1.00  0.00           O
ATOM      8  H   THR     2       0.066  -2.909  -0.796  1.00  0.00           H
ATOM      9  HA  THR     2      -0.197  -0.158  -1.725  1.00  0.00           H
ATOM     10  HB  THR     2      -0.161  -0.405   1.358  1.00  0.00           H
ATOM     11 1HG  THR     2      -2.050   0.808   1.177  1.00  0.00           H
ATOM     12 1HG2 THR     2      -0.231   2.120   1.438  1.00  0.00           H
ATOM     13 2HG2 THR     2       1.279   1.581   0.706  1.00  0.00           H
ATOM     14 3HG2 THR     2      -0.030   2.199  -0.329  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Threonine")
}

fn tryptophane() -> structure::Structure {
    let res = "ATOM      1  N   TRP     2       1.630  -2.708  -0.400  1.00  0.00           N
ATOM      2  CA  TRP     2       2.192  -1.361  -0.429  1.00  0.00           C
ATOM      3  C   TRP     2       3.702  -1.404  -0.428  1.00  0.00           C
ATOM      4  O   TRP     2       4.323  -2.465  -0.405  1.00  0.00           O
ATOM      5  CB  TRP     2       1.688  -0.573   0.812  1.00  0.00           C
ATOM      6  CG  TRP     2       0.182  -0.294   0.826  1.00  0.00           C
ATOM      7  CD1 TRP     2      -0.790  -1.060   1.502  1.00  0.00           C
ATOM      8  CD2 TRP     2      -0.515   0.672   0.130  1.00  0.00           C
ATOM      9  CE2 TRP     2      -1.895   0.486   0.393  1.00  0.00           C
ATOM     10  CE3 TRP     2      -0.079   1.700  -0.745  1.00  0.00           C
ATOM     11  NE1 TRP     2      -2.092  -0.588   1.248  1.00  0.00           N
ATOM     12  CZ2 TRP     2      -2.849   1.335  -0.209  1.00  0.00           C
ATOM     13  CZ3 TRP     2      -1.040   2.530  -1.322  1.00  0.00           C
ATOM     14  CH2 TRP     2      -2.405   2.353  -1.057  1.00  0.00           C
ATOM     15  H   TRP     2       2.209  -3.523  -0.382  1.00  0.00           H
ATOM     16  HA  TRP     2       1.873  -0.855  -1.360  1.00  0.00           H
ATOM     17 2HB  TRP     2       2.208   0.401   0.884  1.00  0.00           H
ATOM     18 3HB  TRP     2       1.972  -1.104   1.742  1.00  0.00           H
ATOM     19 1HD  TRP     2      -0.561  -1.932   2.101  1.00  0.00           H
ATOM     20 1HE  TRP     2      -2.984  -0.976   1.575  1.00  0.00           H
ATOM     21 3HE  TRP     2       0.971   1.839  -0.961  1.00  0.00           H
ATOM     22 2HZ  TRP     2      -3.902   1.197  -0.013  1.00  0.00           H
ATOM     23 3HZ  TRP     2      -0.724   3.323  -1.985  1.00  0.00           H
ATOM     24 2HH  TRP     2      -3.125   3.014  -1.515  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Tryptophane")
}

fn tyrosine() -> structure::Structure {
    let res = "ATOM      1  N   TYR    22      -0.813  -2.199   1.423  1.00  0.00           N
ATOM      2  CA  TYR    22       0.612  -2.082   1.127  1.00  0.00           C
ATOM      3  C   TYR    22       1.441  -2.790   2.171  1.00  0.00           C
ATOM      4  O   TYR    22       0.927  -3.375   3.128  1.00  0.00           O
ATOM      5  CB  TYR    22       1.052  -0.589   1.096  1.00  0.00           C
ATOM      6  CG  TYR    22       0.390   0.302   0.038  1.00  0.00           C
ATOM      7  CD1 TYR    22       0.807   0.254  -1.296  1.00  0.00           C
ATOM      8  CD2 TYR    22      -0.647   1.164   0.406  1.00  0.00           C
ATOM      9  CE1 TYR    22       0.188   1.057  -2.250  1.00  0.00           C
ATOM     10  CE2 TYR    22      -1.264   1.966  -0.549  1.00  0.00           C
ATOM     11  CZ  TYR    22      -0.846   1.912  -1.877  1.00  0.00           C
ATOM     12  OH  TYR    22      -1.452   2.696  -2.817  1.00  0.00           O
ATOM     13  H   TYR    22      -1.176  -2.733   2.289  1.00  0.00           H
ATOM     14  HA  TYR    22       0.816  -2.559   0.150  1.00  0.00           H
ATOM     15 2HB  TYR    22       2.148  -0.527   0.945  1.00  0.00           H
ATOM     16 3HB  TYR    22       0.905  -0.142   2.101  1.00  0.00           H
ATOM     17 1HD  TYR    22       1.605  -0.411  -1.596  1.00  0.00           H
ATOM     18 2HD  TYR    22      -0.984   1.203   1.433  1.00  0.00           H
ATOM     19 1HE  TYR    22       0.504   1.017  -3.282  1.00  0.00           H
ATOM     20 2HE  TYR    22      -2.068   2.623  -0.253  1.00  0.00           H
ATOM     21  HH  TYR    22      -2.136   3.213  -2.386  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Tyrosine")
}

fn valine() -> structure::Structure {
    let res = "ATOM      1  N   VAL     2      -0.090  -2.047  -0.848  1.00  0.00           N
ATOM      2  CA  VAL     2       0.431  -0.683  -0.848  1.00  0.00           C
ATOM      3  C   VAL     2       1.941  -0.681  -0.848  1.00  0.00           C
ATOM      4  O   VAL     2       2.599  -1.729  -0.848  1.00  0.00           O
ATOM      5  CB  VAL     2      -0.132   0.106   0.400  1.00  0.00           C
ATOM      6  CG1 VAL     2       0.469   1.516   0.653  1.00  0.00           C
ATOM      7  CG2 VAL     2      -1.665   0.316   0.369  1.00  0.00           C
ATOM      8  H   VAL     2       0.562  -2.909  -0.848  1.00  0.00           H
ATOM      9  HA  VAL     2       0.107  -0.178  -1.776  1.00  0.00           H
ATOM     10  HB  VAL     2       0.097  -0.501   1.302  1.00  0.00           H
ATOM     11 1HG1 VAL     2       0.314   2.202  -0.201  1.00  0.00           H
ATOM     12 2HG1 VAL     2       0.036   2.003   1.548  1.00  0.00           H
ATOM     13 3HG1 VAL     2       1.558   1.482   0.848  1.00  0.00           H
ATOM     14 1HG2 VAL     2      -2.209  -0.640   0.259  1.00  0.00           H
ATOM     15 2HG2 VAL     2      -2.037   0.772   1.307  1.00  0.00           H
ATOM     16 3HG2 VAL     2      -1.983   0.971  -0.464  1.00  0.00           H
TER
END";
    parser::read_pdb_txt(&res, "Valine")
}
