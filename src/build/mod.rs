//! Build molecules according to a set of existing molecules. This can be Amino Acid, Nucleid Acid or Lipid.
//! Used to test functions or to build examples. It return a [`Structure`] containing the molecule.
//! This cannot be used to create entire protein or entire structure. Just for small molecules.

pub mod residue;

#[doc(inline)]
pub use residue::build_residue;
