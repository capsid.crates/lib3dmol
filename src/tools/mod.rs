//! Various functions to compute distances, apply modifications on structures, extract sequences, ...

pub mod distances;
pub mod tools;

#[doc(inline)]
pub use distances::*;
#[doc(inline)]
pub use tools::*;
