use std::f32;

use crate::structures::*;

/// Calculate the center of mass for structure implementing the GetAtom trait
///
///
/// Atom masses are extracted from CHARMM Force Field .prm files.
/// If CHARMM_FOLDER environement variable is set, .prm files in this folder are used. Else it will be files in datasets folder.
///
/// # Example
/// ```
/// use lib3dmol::{parser, tools};
///
/// let my_prot = parser::read_pdb("tests/tests_file/5jpq.pdb", "5jqp");
///
/// assert_eq!([239.22723, 239.0272, 246.19086], tools::center_of_mass(&my_prot))
/// ```
pub fn center_of_mass<T: GetAtom>(atom_list: &T) -> [f32; 3] {
    let mut coord: [f32; 3] = [0.0, 0.0, 0.0];
    let mut mass_tot: f32 = 0.0;

    for atom in atom_list.get_atom().iter() {
        let mass = atom.get_weight();
        mass_tot += mass;
        coord[0] = coord[0] + mass * atom.coord[0];
        coord[1] = coord[1] + mass * atom.coord[1];
        coord[2] = coord[2] + mass * atom.coord[2];
    }

    [
        coord[0] / mass_tot,
        coord[1] / mass_tot,
        coord[2] / mass_tot,
    ]
}

/// Compute the euclidian distance between 2 atoms
/// $$
/// \sqrt{\sum_{i=1}^{n} (x_i - y_i)^{2} }
/// $$
///
/// # Example
/// ```
/// use lib3dmol::{structures, tools};
/// use lib3dmol::structures::atom::{Atom, AtomType};
///
/// let atom_1 = Atom::new(String::from("HT1"), AtomType::Hydrogen, 1, [0.0, 0.0, 0.0]);
/// let atom_2 = Atom::new(String::from("HT1"), AtomType::Hydrogen, 1, [0.0, 0.0, 0.0]);
/// assert_eq!(tools::euclidian_distance(atom_1.coord, atom_2.coord), 0.0);
/// ```
pub fn euclidian_distance(coord1: [f32; 3], coord2: [f32; 3]) -> f32 {
    ((coord1[0] - coord2[0]).powi(2)
        + (coord1[1] - coord2[1]).powi(2)
        + (coord1[2] - coord2[2]).powi(2))
    .sqrt()
}

/// Select atoms in a radius around a point
/// The atoms at a distance minus or egal to the distance variable will be selected
/// A point is a [f32; 3]
/// atom_list can be all structures which implement the GetAtom trait
/// Return a Vec<&Atom>
///
/// # Example
/// ```
/// use lib3dmol::{structures, tools};
///
///
///
/// ```
pub fn select_atoms_around<'a, T: GetAtom>(
    center: [f32; 3],
    atom_list: &T,
    distance: f32,
) -> Vec<&Atom> {
    atom_list
        .get_atom()
        .into_iter()
        .filter(|x| euclidian_distance(x.coord, center) >= distance)
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn euclidian1() {
        let c1 = [10.2, 14.7, 11.12];
        let c2 = [11.3, 12.01, 15.7];
        assert_eq!(euclidian_distance(c1, c2), 5.4242506);
    }

    #[test]
    fn test_select_atoms_around() {
        use crate::structures::atom::{Atom, AtomType};

        let atom_1 = Atom::new(String::from("HT1"), AtomType::Hydrogen, 1, [1.0, 1.0, 1.0]);
        let atom_2 = Atom::new(String::from("C"), AtomType::Carbon, 2, [10.0, 20.0, 5.0]);
        let atom_3 = Atom::new(String::from("N"), AtomType::Nitrogen, 3, [2.0, 3.0, 6.0]); // Should be a distance of 7

        let all_atoms = vec![atom_1, atom_2, atom_3];

        let my_point = [0.0, 0.0, 0.0];
        assert_eq!(2, select_atoms_around(my_point, &all_atoms, 7.0).len());
    }
}
