use lib3dmol::parser;
use lib3dmol::tools;

#[cfg(test)]
mod test_f2 {
    use super::*;

    #[test]
    fn parse() {
        let my_struct = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
        assert_eq!("f2", my_struct.name());
        assert_eq!(1, my_struct.get_chain_number());
        assert_eq!(66, my_struct.get_residue_number());
        assert_eq!(1085, my_struct.get_atom_number());
    }

    #[test]
    fn molecular_weight() {
        use lib3dmol::structures::GetAtom;
        let my_struct = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
        assert_eq!(7712, my_struct.compute_weight() as u64);
    }

    #[test]
    fn get_residue_ref() {
        let mut my_struct = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
        let chain_a = my_struct.get_chain_ref('A').unwrap();

        let res = chain_a.get_residue_ref(1, None).unwrap();
        assert_eq!("THR", res.name());

        let res = chain_a.get_residue_ref(2, None).unwrap();
        assert_eq!("SER", res.name());

        let res = chain_a
            .get_residue_ref(chain_a.get_number_residue(), None)
            .unwrap();
        assert_eq!("HSD", res.name());
    }

    #[test]
    fn compute_mass_center() {
        let my_struct = parser::read_pdb("tests/tests_file/f2.pdb", "f2");

        assert_eq!(
            [0.00042509945, 19.999798, -0.00044671507],
            tools::center_of_mass(&my_struct)
        )
    }
}

#[test]
fn parse_f2_icode() {
    let my_struct = parser::read_pdb("tests/tests_file/f2_icode.pdb", "f2");
    assert_eq!(1, my_struct.get_chain_number());
    assert_eq!(67, my_struct.get_residue_number());
    assert_eq!(1096, my_struct.get_atom_number());
}

#[test]
fn parse_f2_adn() {
    let my_struct = parser::read_pdb("tests/tests_file/f2_adn.pdb", "f2");
    assert_eq!(5, my_struct.get_chain_number());
    assert_eq!(8382, my_struct.get_residue_number());
    assert_eq!(47740, my_struct.get_atom_number());
}

#[test]
fn parse_trp() {
    let my_struct = parser::read_pdb("tests/tests_file/trp_MD.pdb", "trp");
    assert_eq!(3, my_struct.get_chain_number());
    assert_eq!(9970, my_struct.get_residue_number());
    assert_eq!(396_109, my_struct.get_atom_number());
}

#[test]
fn parse_5jpq() {
    let my_struct = parser::read_pdb("tests/tests_file/5jpq.pdb", "5jpq");
    assert_eq!(56, my_struct.get_chain_number());
    assert_eq!(15066, my_struct.get_residue_number());
    assert_eq!(95839, my_struct.get_atom_number());
}

#[cfg(test)]
mod test_tools {
    use super::*;
    #[test]
    fn remove_hydrogens_trp() {
        let mut my_struct = parser::read_pdb("tests/tests_file/trp_MD.pdb", "trp");
        assert_eq!(396_109, my_struct.get_atom_number());
        my_struct.remove_h();
        assert_eq!(133_831, my_struct.get_atom_number());
    }

    #[test]
    fn remove_hydrogens_f2() {
        let mut my_struct = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
        assert_eq!(1085, my_struct.get_atom_number());
        my_struct.remove_h();
        assert_eq!(541, my_struct.get_atom_number());
    }

    #[test]
    fn test_rmsd() {
        use lib3dmol::structures::atom::{Atom, AtomType};
        let lst_atom_1 = vec![Atom::new(
            String::from("H"),
            AtomType::Hydrogen,
            1,
            [0.0, 0.0, 0.0],
        )];
        let lst_atom_2 = vec![Atom::new(
            String::from("H"),
            AtomType::Hydrogen,
            1,
            [0.6, 0.3, 0.2],
        )];
        assert_eq!(0.7, tools::rmsd(&lst_atom_1, &lst_atom_2).unwrap());
    }

    #[test]
    fn compute_weight_5jpq() {
        use lib3dmol::structures::GetAtom;
        let my_struct = parser::read_pdb("tests/tests_file/5jpq.pdb", "5jpq");

        assert_eq!(1294368.6, my_struct.compute_weight());
    }
    #[test]
    fn mass_center_5jpq() {
        let my_struct = parser::read_pdb("tests/tests_file/5jpq.pdb", "5jpq");

        assert_eq!(
            [239.22723, 239.0272, 246.19086],
            tools::center_of_mass(&my_struct)
        )
    }
}
